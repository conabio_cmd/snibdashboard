''' Index
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-06
'''
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

from app import app
from apps import (conteos,
                  mapas,
                  tendencias)

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.H2("Explora SNIB", className="display-4"),
        html.Hr(),
        html.P(
            "Un simple explorador de datos del Sistema Nacional de Información sobre la Biodiversidad", className="lead"
        ),
        dbc.Nav([
            dbc.NavLink("Conteos", href="/conteos/", id="conteos-link"),
            dbc.NavLink("Mapas", href="/mapas", id="mapas-link"),
            dbc.NavLink("Tendencias", href="/tendencias", id="tendencias-link")
            ],
                vertical=True,
                pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = dbc.Container([dcc.Location(id='url', refresh=False),
                            sidebar,
                            content])


@app.callback(
    Output('page-content', 'children'),
    [Input('url', 'pathname')]
)
def display_page(pathname):
    if pathname in ['/conteos', '/conteos/']:
        return conteos.layout

    if pathname in ['/mapas', '/mapas/']:
        return mapas.layout

    if pathname in ['/tendencias', '/tendencias/']:
        return tendencias.layout

    return dbc.Jumbotron([
        html.H1('404: Página no encontrada'),
        html.Hr(),
        html.H3(f'No se pudo encontrar {pathname[1:]}', className='text-muted')
    ])


if __name__ == '__main__':
    app.run_server(debug=True)
