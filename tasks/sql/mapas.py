''' Consultas para datos de mapas
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-27
'''

# Se obitiene el id del estado y el numero de ejemplares en el
CONTEOS_ESTADOS = '''
    SELECT
        DISTINCT
            idestadomapa,
            estadomapa,
            paismapa,
            COUNT(*) AS count
    FROM informaciongeoportal_siya
    WHERE paismapa = 'MEXICO'
        AND estadomapa != ''
    GROUP BY estadomapa, paismapa
'''

