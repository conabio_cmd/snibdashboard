''' Consultas de actualizacion para tendencias
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-31
'''

# Consulta por estados y aniocolecta
CONTEOS_X_ESTADO_ANIOCOLECTA = '''
    SELECT
        DISTINCT
            estadomapa AS estado,
            aniocolecta,
            COUNT(*) AS conteo
    FROM informaciongeoportal_siya
    WHERE paismapa = 'MEXICO'
    GROUP BY
        estadomapa,
        paismapa,
        aniocolecta
'''

