'''
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-31
'''
from tasks.sql.conteos import (CONTEOS_X_GRUPO_ESTADO,
                     CONTEOS_X_COLECCION_TOP10)
from tasks.sql.mapas import (CONTEOS_ESTADOS)
from tasks.sql.tendencias import (CONTEOS_X_ESTADO_ANIOCOLECTA)

__all__ = [
    CONTEOS_ESTADOS,
    CONTEOS_X_GRUPO_ESTADO,
    CONTEOS_X_COLECCION_TOP10,
    CONTEOS_X_ESTADO_ANIOCOLECTA
]
