''' Consultas sobre conteos
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-27
'''

# Consulta sobre el numero de ejemplares por grupo en cada estado
CONTEOS_X_GRUPO_ESTADO = '''
SELECT
    DISTINCT grupobio AS grupo,
    estadomapa AS estado,
    COUNT(*) AS conteo
FROM informaciongeoportal_siya
WHERE paismapa = 'MEXICO'
GROUP BY grupobio, estadomapa
'''

# Consulta sobre las 10 colecciones con mas ejemplares reportados
CONTEOS_X_COLECCION_TOP10 = '''
SELECT
    DISTINCT coleccion,
    COUNT(*) AS conteo
FROM informaciongeoportal_siya
WHERE coleccion NOT IN('NO APLICA', 'NO DISPONIBLE')
GROUP BY coleccion
ORDER BY conteo DESC
LIMIT 10
'''
