''' Tareas de actualizacion de datos de SNIB
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-24
'''
import datetime
import pandas as pd
import redis
import sqlalchemy as sa

from app import celery as celery_app
from tasks import sql


REDIS_HASHNAME = 'snib-data'
REDIS_KEYS = {
    'CONTEOS': 'CONTEOS',
    'COLECCIONES': 'COLECCIONES',
    'TENDENCIAS': 'TENDENCIAS',
    'MAPAS_DATA': 'MAPAS_DATA',
    'DATE_UPDATED': 'DATE_UPDATED'
}


@celery_app.on_after_configure.connect
def setup_periodic_task(sender, **kwargs):
    print('---> Setup periodic tasks')
    sender.add_periodic_task(
        30,
        update_data.s(),
        name='Update_data',
    )


@celery_app.task
def update_data():
    print('---> Update data')

    redis_instance = celery_app.conf.redisinstance
    with celery_app.conf.dbengine.connect() as conn:
        col_data = pd.read_sql_query(sql.CONTEOS_X_COLECCION_TOP10, conn)
        snib_data = pd.read_sql_query(sql.CONTEOS_X_GRUPO_ESTADO, conn)
        mapas_data = pd.read_sql_query(sql.CONTEOS_ESTADOS, conn)
        tendencias_data = pd.read_sql_query(sql.CONTEOS_X_ESTADO_ANIOCOLECTA,
                                            conn)

    redis_instance.hset(REDIS_HASHNAME,
                        REDIS_KEYS['CONTEOS'],
                        snib_data.to_json(orient='split'))

    redis_instance.hset(REDIS_HASHNAME,
                        REDIS_KEYS['COLECCIONES'],
                        col_data.to_json(orient='split'))

    redis_instance.hset(REDIS_HASHNAME,
                        REDIS_KEYS['MAPAS_DATA'],
                        mapas_data.to_json(orient='split'))

    redis_instance.hset(REDIS_HASHNAME,
                        REDIS_KEYS['TENDENCIAS'],
                        tendencias_data.to_json(orient='split'))

    redis_instance.hset(REDIS_HASHNAME,
                        REDIS_KEYS['DATE_UPDATED'],
                        str(datetime.datetime.now()))
