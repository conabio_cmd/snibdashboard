# Dashboard para la exploración de datos del SNIB

El Sistema Nacional de Información sobre la Biodiversidad (SNIB) es la 
principal base de conocimiento sobre la biodiverisdad mexicana. Ésta es 
mantenida por CONABIO.

Este proyecto tiene como finalidad presentar algunas estadísticas sobre la 
composición del SNIB. 

El desarrollo del servicio está basado en [Dash], e incluye un servicio de 
cache de datos en Redis el cual es actualizado usando [Celery].

## Configuración para ejecución

Para poder ejecutar el servicio es necesario definir las siguientes variables
de entorno:

 - `SNIB_DB_URL`, dirección del servidor donde se encuentra la base de datos 
 del SNIB, en particular se necesita la tabla `informaciongeoportal_siya`. La
 dirección se escribe como: 
 `dialect+driver://username:password@host:port/database`.
 - `REDIS_URL`, dirección de un servidor de Redis el cual será usado para 
 guardar los datos intermedios de la aplicación de [Dash].
 - `MAPBOX_TOKEN`, token del servicio de [Mapbox].

Por ejemplo usando el archivo `env.example` se puede hacer en una terminal de
Bash lo siguiente:

    $ source env.example

con lo que se definiran las variables de entorno necesarias. Después se debe
ejecutar tres procesos. Un servicio de planificación, un _worker_ y el servicio
de [Dash]. Para esto ejecutamos 

    $ pipenv run celery -A tasks.update_data beat --loglevel=DEBUG
    $ pipenv run celery -A tasks.update_data worker --loglevel=DEBUG -P gevent
    $ pipenv run python index.py

Cada uno de estos comando se puede ejecutar en segundo plano. Note que se usa
un entorno de `pipenv`, el cual se debe de crear antes de ejecutar los servicios.

## Organización del desarrollo

El punto de entrada de la aplicación es el archivo `index.py`, ahí se tiene la
definición de las páginas que conforman la aplicación y la plantilla básica de 
la misma. 

Se decidió que cada página fuera un módulo separado, además cada una de éstas 
trata de información distinta, las páginas se encuentran el directorio `apps/`. 

Las tareas de actualización de los datos se encuentran en la carpeta de `tasks/`.
Actualmente solo hay una llamada `update_data`. 

En el archivo `app.py` se tiene lo relativo al servicio en general, ahí se 
define las conexiones y el contexto general de las aplicaciciones [Dash] y 
[Celery].

[Dash]: https://plotly.com/dash/
[Celery]: http://celeryproject.org
[Mapbox]: https://www.mapbox.com/
