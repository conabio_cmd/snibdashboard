''' Funciones de utilidades para SNIB Dashboard
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-26
'''
import os
from celery import Celery


def make_celery(app):
    '''Crea una aplicación de Celery dada un app de Dash

    Args:
        app (dash.Dash): Aplicación para la cual se neceista un app de Celery

    Returns:
        celery.Celery: Regresa una aplicación de celery con el contexto de Dash
    '''
    celery = Celery(app.config.name,
                    backend=app.config.get('celery_result_backend', None),
                    broker=app.config.get('celery_broker_url', None))

    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.server.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


def check_environment():
    '''Revisa que las variables de entorno estén definidas

    Raises:
        KeyError
    '''
    ENV_VARS = [
        'SNIB_DB_URL',
        'REDIS_URL',
        'MAPBOX_TOKEN'
    ]

    for var in ENV_VARS:
        env_var = os.getenv(var)
        if env_var == None:
            raise KeyError(f'Environment varible {var} is not set')
