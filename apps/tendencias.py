''' Análisis de tendencias de colectas

Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-12
'''
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px

from datetime import date
import pandas as pd
from app import app

redis_instance = app.config.redisinstance
REDIS_HASHNAME = 'snib-data'

data = pd.read_json(
        redis_instance.hget(REDIS_HASHNAME, 'TENDENCIAS'),
        orient='split')

# DATA MUNGING ----
# Remove no data
data.loc[data['aniocolecta'] == 9999, 'aniocolecta'] = None
data = data.astype({'aniocolecta': 'Int64'})
data = data[~data['aniocolecta'].isna()]
# Remove estadomapa vacio
data = data[data['estado'] != ""]

data.set_index(['estado', 'aniocolecta'], inplace=True)
# Add cummulative counts
data = data.assign(
    acumulado=data.groupby(level='estado')['conteo'].cumsum()
)
# Add growth ratio
data = data.assign(
        crecimiento=data.groupby(
            level='estado'
        )['acumulado'].transform(lambda x: x/x.max())
)

data.reset_index(inplace=True)
# END DATA MUNGING ----

current_year = int(date.today().strftime('%Y'))
slider_labels = {y: str(y) for y in range(1900, current_year+1, 15)}
layout = html.Div([
    html.H2('Temporalidad de las colectas dentro del SNIB'),
    html.Label(
        'Selecciona un rango de años para filtar los conteos de registros'
    ),
    dcc.RangeSlider(
        id='year-filter',
        min=1900,
        max=current_year,
        step=15,
        value=[1940, current_year],
        marks=slider_labels
    ),
    dcc.Graph(id='tendencia'),
    dcc.Graph(id='growth'),
    dcc.Graph(id='heatmap'),
    html.Div(id='data-share', style={'display': 'none'})
])

@app.callback(
    Output('data-share', 'children'),
    [Input('year-filter', 'value')]
)
def filter_data(value):
    min_year, max_year = value
    return data[(data['aniocolecta']>=min_year) &
                (data['aniocolecta']<=max_year)].to_json(orient='split')


@app.callback(
    Output('tendencia', 'figure'),
    [Input('data-share', 'children')]
)
def plot_tendencia(jsonData):
    mydata = pd.read_json(jsonData, orient='split')
    fig = px.line(mydata,
                  x='aniocolecta',
                  y='acumulado',
                  color='estado',
                  labels={
                      'acumulado': 'No de registros (acumulados)',
                      'aniocolecta': 'Año de colecta'
                      })
    return fig


@app.callback(
    Output('growth', 'figure'),
    [Input('data-share', 'children')]
)
def plot_growth_ratio(jsonData):
    mydata = pd.read_json(jsonData, orient='split')
    fig = px.line(mydata,
                  x='aniocolecta',
                  y='crecimiento',
                  color='estado',
                  labels={
                      'aniocolecta': 'Año de colecta',
                      'crecimiento': 'Porcentaje de registros reportados'
                    })
    return fig


@app.callback(
    Output('heatmap', 'figure'),
    [Input('data-share', 'children')]
)
def plot_conteos_agregados(jsonData):
    mydata = pd.read_json(jsonData, orient='split')
    mydata.loc[mydata['aniocolecta']<=1890] = 1890

    fig = px.density_heatmap(mydata,
                             x='aniocolecta',
                             y='estado',
                             z='crecimiento',
                             histfunc='max',
                             nbinsx=26,
                             labels={
                                 'aniocolecta': 'Año de colecta',
                                 'estado': 'Estado',
                                 'creciemiento': None})
    # fig.update_layout(showlegend=False, overwrite=True)
    # fig.update_layout(legend={'title': 'Porcentaje de crecimiento'})

    return fig
