''' Aplicacion sobre conteos de SNIB
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-06
'''
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px

import pandas as pd

from app import app

redis_instance = app.config.redisinstance
REDIS_HASHNAME = 'snib-data'

col_data = pd.read_json(
        redis_instance.hget(REDIS_HASHNAME, 'COLECCIONES'),
        orient='split')
snib_data = pd.read_json(
        redis_instance.hget(REDIS_HASHNAME, 'CONTEOS'),
        orient='split')

col_fig = px.bar(col_data,
                 x='coleccion',
                 y='conteo',
                 labels={
                     'coleccion': 'Colección',
                     'conteo': 'No. de registros'
                 })

state_opts = [{'label': state, 'value': state}
              for state in snib_data['estado'].unique()
              if state != '']
state_opts.append({'label': 'SELECIONA UN ESTADO...', 'value': 'TOTAL'})

layout = html.Div([
    html.H2('Conteo de registros por grupo Taxonómico'),
    dbc.Row([
        dbc.Col(dcc.Dropdown(id='stateSelection',
                             options=state_opts,
                             value='TOTAL'), className='col-8'),
        dbc.Col(
            dbc.RadioItems(
                id='ratio',
                options=[
                    {'label': 'Conteos', 'value': 0},
                    {'label': 'En porcentajes', 'value': 1}
                ],
                value=0
            ),
            className='col-md-auto'
        )
    ]),
    html.P(className='text-right lead', id='occurrencesCount'),
    dcc.Graph(id='stateOccurrences'),
    # Hidden div
    html.Div(id='computedData', style={'display': 'none'}),
    html.H2('Colecciones con mayor número de registros'),
    dcc.Graph(id='colecciones', figure=col_fig),
])


@app.callback(
    Output('computedData', 'children'),
    [Input('stateSelection', 'value')]
)
def update_data_by_state(state):
    if state == 'TOTAL':
        cleaned_data = snib_data.groupby('grupo').sum()
        cleaned_data.reset_index(inplace=True)
    else:
        cleaned_data = snib_data[snib_data['estado'] == state]

    return cleaned_data.to_json(orient='split')


@app.callback(
    Output('occurrencesCount', 'children'),
    [Input('computedData', 'children')]
)
def count_occurrences(jsonData):
    data = pd.read_json(jsonData, orient='split')
    count = data['conteo'].sum()

    return f'No de registros: {count:,}'


@app.callback(
    Output('stateOccurrences', 'figure'),
    [Input('computedData', 'children'),
     Input('ratio', 'value')]
)
def update_occurrrences_fig(jsonData, inRatio=0):
    data = pd.read_json(jsonData, orient='split')
    ylabel = 'No de registros'

    if inRatio:
        ylabel = 'Porcentaje de registros'
        data.loc[:, 'conteo'] = data['conteo']/data['conteo'].sum()*100

    fig = px.bar(data[data['grupo'] != 'NO DISPONIBLE'],
                 x='grupo',
                 y='conteo',
                 labels={
                     'grupo': 'Grupo biológico',
                     'conteo': ylabel
                 })

    return fig
