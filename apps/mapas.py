''' Mapas sobre conteos del SNIB
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-10
'''
import json
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px

import pandas as pd
from app import app
MAPBOX_TOKEN = app.config['token']

redis_instance = app.config.redisinstance
REDIS_HASHNAME = 'snib-data'

with open('./assets/mexico_dv.json', 'r') as f:
    mexico_dv = json.load(f)

for el in mexico_dv['features']:
    value = int(el['properties']['CVE_EDO'])
    el['properties']['CVE_EDO'] = value

data = pd.read_json(
        redis_instance.hget(REDIS_HASHNAME, 'MAPAS_DATA'),
        orient='split')

# app.logger.debug(data)
# app.logger.debug(mexico_dv['features'][0]['properties'])
# app.logger.debug(MAPBOX_TOKEN)

fig = px.choropleth_mapbox(
        data_frame=data,
        geojson=mexico_dv,
        featureidkey='properties.CVE_EDO',
        locations='idestadomapa',
        color='count',
        color_continuous_scale='Viridis',
        opacity=0.7,
        range_color=(35_000, 1_400_000),
        zoom=3,
        center={'lat': 23.63,'lon': -102.55},
        labels={'count': 'No. de registros'})
fig.update_layout(mapbox_style='light',
        mapbox_accesstoken=MAPBOX_TOKEN)


layout = html.Div([
    html.H1('Mapas'),
    dcc.Graph(id='mapa', figure=fig)
])
