''' SNIB dashboard de exploración
Author: Juan M. Barrios <juan.barrios@conabio.gob.mx>
Date: 2020-08-06
'''
from logging.config import dictConfig
import os

import dash
import dash_bootstrap_components as dbc

import sqlalchemy as sa
import redis
from utils import (check_environment,
                   make_celery)

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'console': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://sys.stdout',
        'formatter': 'default',
    }},
    'root': {
        'level': 'DEBUG',
        'handlers': ['console'],
    }
})

check_environment()
DB_URL = os.getenv('SNIB_DB_URL')
db_engine = sa.create_engine(DB_URL)

REDIS_URL = os.getenv('REDIS_URL')
redis_instance = redis.Redis.from_url(REDIS_URL)

MAPBOX_TOKEN = os.getenv('MAPBOX_TOKEN')
app = dash.Dash(name=__name__,
                title='SNIB Dashboard',
                external_stylesheets=[dbc.themes.FLATLY],
                suppress_callback_exceptions=True)

app.config.update(
    dbengine=db_engine,
    redisinstance=redis_instance,
    token=MAPBOX_TOKEN,
    celery_broker_url=REDIS_URL
)

celery = make_celery(app)
